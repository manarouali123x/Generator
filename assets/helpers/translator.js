const ExpressTranslate = require('express-translate');
const expressTranslate = new ExpressTranslate();
expressTranslate.addLanguage('en', require('../../translations/en.json'));

module.exports = expressTranslate.translator('en');
