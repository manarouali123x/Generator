const Generator = require('../../abstractGenerator');
const {randomElement, randomise, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');
const Ean13Barcode = require('../../barcodes/ean13');

module.exports = class ISBN extends Generator {
    static get weights() {
        return {
            10: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            13: [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3]
        }
    }

    static get countries() {
        return {'_': [92],
            'AD': [99913,99920],
            'AG': [976],
            'AL': [99927],
            'AM': [5,99930],
            'AR': [950,987],
            'AT': [3],
            'AU': [0,1],
            'AW': [99904],
            'AZ': [5,9952],
            'BA': [86,9958],
            'BB': [976],
            'BD': [984],
            'BE': [2],
            'BG': [954],
            'BH': [99901],
            'BJ': [99919],
            'BN': [99917],
            'BR': [85],
            'BS': [976],
            'BT': [99936],
            'BW': [99912],
            'BY': [5,985],
            'BZ': [976],
            'CA': [0,1,2],
            'CH': [2,3,88],
            'CK': [982],
            'CL': [956],
            'CM': [9956],
            'CN': [7],
            'CO': [958],
            'CR': [9968,9977],
            'CU': [959],
            'CY': [9963],
            'CZ': [80],
            'DE': [3],
            'DK': [87],
            'DM': [976],
            'DO': [99935],
            'DZ': [9961],
            'EC': [9978],
            'EE': [5,9985],
            'EG': [977],
            'ES': [84],
            'FI': [951,952],
            'FJ': [982],
            'FO': [99918],
            'FR': [2, '979-10'],
            'GB': [0,1],
            'GD': [976],
            'GE': [5,99928],
            'GH': [9964,9988],
            'GM': [9983],
            'GR': [960],
            'GT': [99922],
            'GY': [976],
            'HK': [962],
            'HN': [99926],
            'HR': [86,953],
            'HU': [963],
            'ID': [979],
            'IE': [0,1],
            'IL': [965],
            'IN': [81,93],
            'IR': [964],
            'IS': [9979],
            'IT': [88, '979-12'],
            'JM': [976],
            'JO': [9957],
            'JP': [4],
            'KE': [9966],
            'KG': [5,9967],
            'KI': [982],
            'KN': [976],
            'KR': [89, '979-11'],
            'KZ': [5,9965],
            'LB': [9953],
            'LC': [976],
            'LK': [955],
            'LS': [99911],
            'LT': [5,9955,9986],
            'LU': [2],
            'LV': [5,9984],
            'LY': [9959],
            'MA': [9954,9981],
            'MD': [5,9975],
            'MK': [86,9989],
            'MN': [99929],
            'MO': [99937],
            'MS': [976],
            'MT': [99909,99932],
            'MU': [99903],
            'MV': [99915],
            'MW': [99908],
            'MX': [968,970],
            'MY': [967,983],
            'NA': [99916],
            'NG': [978],
            'NI': [99924],
            'NL': [90,99904],
            'NO': [82],
            'NP': [99933],
            'NR': [982],
            'NU': [982],
            'NZ': [0,1],
            'PA': [9962],
            'PG': [9980],
            'PH': [971],
            'PK': [969],
            'PL': [83],
            'PR': [0,1],
            'PT': [972],
            'PY': [99925],
            'QA': [99921],
            'RO': [973],
            'RU': [5],
            'SA': [9960],
            'SB': [982],
            'SC': [99931],
            'SE': [91],
            'SG': [981,9971],
            'SI': [86,961],
            'SK': [80],
            'SL': [99910],
            'SR': [99914],
            'SV': [99923],
            'SY': [9972],
            'SZ': [0,1],
            'TH': [974],
            'TJ': [5],
            'TK': [982],
            'TM': [5],
            'TN': [9973],
            'TO': [982],
            'TR': [975],
            'TT': [976],
            'TV': [982],
            'TW': [957,986],
            'TZ': [9976,9987],
            'UA': [5,966],
            'UG': [9970],
            'US': [0,1],
            'UY': [9974],
            'UZ': [5],
            'VC': [976],
            'VE': [980],
            'VU': [982],
            'WS': [982],
            'ZA': [0,1],
            'ZM': [9982],
            'ZW': [0,1]
        }
    }

    params() {
        return {
            country: {
                type: 'string',
                values: Object.keys(ISBN.countries),
            },
            length: {
                type: 'int',
                values: [10, 13],
                default: 13,
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);
        const countryCode = params.country || randomElement(Object.keys(ISBN.countries));

        let bcode = randomElement(ISBN.countries[countryCode]);

        if (parseInt(params['length']) === 10) {
            if (typeof bcode === 'string') {
                bcode = ISBN.countries[countryCode][0];
            }

            let nr = randomise(digits, 9 - bcode.toString().length);

            let checksum = sum(arrayTimesArray(ISBN.weights[10], bcode + nr)) % 11;
            checksum = checksum === 10 ? 'X' : checksum;

            return `${bcode}-${nr}-${checksum}`;
        }

        const [prefix, bcode2] = typeof bcode === 'string' ? bcode.split('-') : [978, bcode.toString()];

        const nr = randomise(digits, 9 - bcode2.length);
        const checksum = (10 - (sum(arrayTimesArray(ISBN.weights[13], prefix + bcode2 + nr)) % 10)) % 10;

        return `${prefix}-${bcode2}-${nr}-${checksum}`;
    }

    validate(value) {
        value = value.replace(/[- ]/g, '');

        const matchingCountries = [];
        if (value.length === 10) {
            if (!value.match(/^\d{9}(\d|X)$/g)) {
                throw new Error('format')
            }

            const checksum = sum(arrayTimesArray(ISBN.weights[10], value.slice(0, 9))) % 11;

            const givenChecksum = value[9] === 'X' ? 10 : parseInt(value[9]);

            if (checksum !== givenChecksum) {
                throw new Error('checksum')
            }

            for (let countryCode in ISBN.countries) {
                if (ISBN.countries.hasOwnProperty(countryCode)) {
                    const bcodes = ISBN.countries[countryCode];
                    for (let bcode of bcodes) {
                        if (typeof bcode === 'string') {
                            continue;
                        }
                        bcode = bcode.toString();

                        if ((value.substr(0, bcode.length) === bcode) && (matchingCountries.indexOf(countryCode) === -1)) {
                            matchingCountries.push(countryCode);
                        }
                    }
                }
            }

            return {
                countries: matchingCountries,
            };
        }

        if (value.length === 13) {
            if (!value.match(/^97(8|9)\d{10}$/g)) {
                throw new Error('format')
            }

            const checksum = (10 - (sum(arrayTimesArray(ISBN.weights[13], value.slice(0, 12))) % 10)) % 10;
            if (checksum  !== parseInt(value[12])) {
                throw new Error('checksum')
            }

            for (let countryCode in ISBN.countries) {
                if (ISBN.countries.hasOwnProperty(countryCode)) {
                    const bcodes = ISBN.countries[countryCode];
                    for (let bcode of bcodes) {
                        bcode = typeof bcode === 'string' ? bcode.replace(/-/g, '') : `978${bcode}`;

                        if ((value.substr(0, bcode.length) === bcode) && (matchingCountries.indexOf(countryCode) === -1)) {
                            matchingCountries.push(countryCode);
                        }
                    }
                }
            }

            return {
                countries: matchingCountries,
                barcode: (new Ean13Barcode(value)).toSvg(),
            };
        }

        throw new Error('format')
    }
};
