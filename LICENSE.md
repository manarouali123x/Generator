const Generator = require('../../abstractGenerator');
const {randomElement, randRange} = require('../../helpers');
const handleParams = require('../../handleParams');

module.exports = class SSN extends Generator {
    static get regions() {
        return {
            "AL": [[416, 424]],
            "AK": [574],
            "AS": [586],
            "AZ": [[526, 527]],
            "AR": [[429, 432]],
            "CA": [[545, 573], [587, 665]],
            "CO": [[521, 524]],
            "CT": [[40, 49]],
            "DE": [[221, 222]],
            "DC": [[577, 579]],
            "FM": [586],
            "FL": [[261, 267]],
            "GA": [[252, 260]],
            "GU": [586],
            "HI": [[575, 576]],
            "ID": [[518, 519]],
            "IL": [[318, 361]],
            "IN": [[303, 317]],
            "IA": [[478, 485]],
            "KS": [[509, 515]],
            "KY": [[400, 407]],
            "LA": [[433, 439]],
            "ME": [[4, 7]],
            "MH": [586],
            "MD": [[212, 220]],
            "MA": [[10, 34]],
            "MI": [[362, 386]],
            "MN": [[468, 477]],
            "MS": [[425, 428]],
            "MO": [[486, 500]],
            "MT": [[516, 517]],
            "NE": [[505, 508]],
            "NV": [520, 680],
            "NH": [[1, 3]],
            "NJ": [[135, 158]],
            "NM": [525, 585],
            "NY": [[50, 134]],
            "NC": [232, [237, 246]],
            "ND": [[501, 502]],
            "MP": [586],
            "OH": [[268, 302]],
            "OK": [[440, 448]],
            "OR": [[540, 544]],
            "PW": [586],
            "PA": [[159, 211]],
            "PR": [[580, 584]],
            "RI": [[35, 39]],
            "SC": [[247, 251]],
            "SD": [[503, 504]],
            "TN": [[408, 415]],
            "TX": [[449, 467]],
            "UT": [[528, 529]],
            "VT": [[8, 9]],
            "VI": [580],
            "VA": [[223, 231]],
            "WA": [[531, 539]],
            "WV": [[232, 236]],
            "WI": [[387, 399]],
            "WY": [520],
            "_RAIL": [[700, 728]],
            "_ENUM": [[729, 730]],
        }
    }

    params() {
        return {
            area: {
                type: 'string',
                values: Object.keys(SSN.regions),
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        let area = undefined;
        if (params.area) {
            const range = randomElement(SSN.regions[params.area]);
            area = typeof(range) === 'number' ? range : randRange(range[0], range[1] + 1);
        } else {
            while (true) {
                area = randRange(1, 731);
                const states = this._getMatchingAreas(area);
                if (states.length > 0) {
                    break;
                }
            }
        }

        const group = randRange(1, 100);
        const serial = randRange(1, 10000);

        return `${area.toString().padStart(3, '0')}-${group.toString().padStart(2, '0')}-${serial.toString().padStart(4, '0')}`
    }

    validate(value) {
        const match = value.match(/^(\d{3})-(\d{2})-(\d{4})$/);
        if (!match) {
            throw new Error('format');
        }

        const area = parseInt(match[1]);
        const group = parseInt(match[2]);
        const serial = parseInt(match[3]);
        const states = this._getMatchingAreas(area);
        if (states.length === 0) {
            throw new Error('region');
        }

        if (area === 0 || group === 0 || serial === 0) {
            throw new Error('format');
        }

        return {
            regions: states.map(stateCode => ['US', stateCode]),
        };
    }

    _getMatchingAreas(areaCode) {
        const matching = [];
        for (let code in SSN.regions) {
            if (SSN.regions.hasOwnProperty(code)) {
                if (this._matchesArea(areaCode, SSN.regions[code])) {
                    matching.push(code);
                }
            }
        }
        return matching;
    }

    _matchesArea(value, ranges) {
        for (let range of ranges) {
            if (typeof(range) === 'number') {
                if (range === value) {
                    return true;
                }
            }
            if (value >= range[0] && value <= range[1]) {
                return true;
            }
        }

        return false;
    }
};
